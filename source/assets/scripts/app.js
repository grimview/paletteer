const remote = require('electron').remote
const {clipboard, Menu, MenuItem} = remote

const Paletteer = require('./Paletteer.js')

const Conversion = require('./components/Conversion.js')

const LEFT = 37; const RIGHT = 39

function main () {
  let elements = {
    subpalettes: document.getElementById('subpalettes'),
    palette: document.getElementById('palette'),
    indicator: document.getElementById('indicator'),
    mode: document.getElementById('mode-toggler'),
    actions: {
      addColour: document.getElementById('add-colour'),
      removeColour: document.getElementById('remove-colour'),
      newPalette: document.getElementById('new-palette'),
      deletePalette: document.getElementById('delete-subpalette')
    },
    labels: [ document.getElementById('option-one-label'),
      document.getElementById('option-two-label'),
      document.getElementById('option-three-label')
    ],
    info: [ document.getElementById('option-one-value'),
      document.getElementById('option-two-value'),
      document.getElementById('option-three-value')
    ],
    sliders: [ document.getElementById('option-one-slider'),
      document.getElementById('option-two-slider'),
      document.getElementById('option-three-slider') ]
  }

  var paletteer = new Paletteer(elements)

  paletteer.start()

  // * Keyboard navigation for Paletteer.
  document.addEventListener('keydown', (event) => {
    let allowedToContinue = true

    elements.sliders.forEach((slider, sliderIndex) => {
      if (slider === document.activeElement) {
        allowedToContinue = false
      }
    })

    if (allowedToContinue) {
      switch (event.keyCode) {
        case LEFT:
          if (paletteer.current.colour > 0) {
            paletteer.current.colour--
            paletteer.update()
          }
          break

        case RIGHT:
          if (paletteer.current.colour < paletteer.colours.length - 1) {
            paletteer.current.colour++
            paletteer.update()
          }
          break

        case 67:
          if (paletteer.options.mode === 0) {
            clipboard.writeText(Conversion.hslToString(paletteer.colours[paletteer.current.colour].hsl))
          } else {
            clipboard.writeText(paletteer.colours[paletteer.current.colour].element.style.backgroundColor)
          }

          paletteer.colours[paletteer.current.colour].element.classList.add('copied')

          setTimeout(() => {
            paletteer.colours[paletteer.current.colour].element.classList.remove('copied')
          }, 200)
          break
      }
    }
  })

  document.addEventListener('contextmenu', (e) => {
    let menu = new Menu()
    let menuItem

    if (paletteer.options.mode === 0) {
      menuItem = new MenuItem({
        label: 'Copy HSL Code',
        click: () => {
          clipboard.writeText(Conversion.hslToString(paletteer.colours[paletteer.current.colour].hsl))
        }
      })
    } else {
      menuItem = new MenuItem({
        label: 'Copy RGB Code',
        click: () => {
          clipboard.writeText(paletteer.colours[paletteer.current.colour].element.style.backgroundColor)
        }
      })
    }

    menu.append(menuItem)

    if (e.target.classList.contains('colour')) {
      e.preventDefault()

      menu.popup(remote.getCurrentWindow())
    }
  }, false)
}

main()
