const Conversion = require('./source/assets/scripts/components/Conversion')

describe('Paletteer Tests', () => {
  it('Convert the string, rgba(120, 56, 92), to [120, 56, 92].', () => {
    expect(Conversion.stringToRgb('rgb(120, 56, 92)')).toEqual([120, 56, 92])
  })
})
